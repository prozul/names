# configures development virtual machine
node 'names.dev.local' {

  include stdlib

  # read settings yaml
  $settings = loadyaml('/vagrant/provision/settings.yaml')
  $packages = $settings[packages]
  $versions = $settings[versions]

  # install software packages
  package { $packages:
    ensure => present,
  }

  # start in /vagrant
  file_line { 'start-dir':
    path => '/home/vagrant/.bashrc',
    line => 'cd /vagrant',
  }

  # install docker and docker-compose
  class { 'docker':
    version      => $versions[docker],
    docker_users => ['vagrant'],
  }

  class { 'docker::compose':
    version => $versions[compose],
    ensure  => present,
  }

  # alias for docker-compose
  file { '/usr/local/bin/dc':
    ensure => 'link',
    target => '/usr/local/bin/docker-compose',
  }

  # install kubectl
  yumrepo { 'kubernetes-yum-repo':
    ensure        => present,
    descr         => 'Kubectl YUM repository',
    baseurl       => 'https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64',
    gpgkey        => 'https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg',
    enabled       => 1,
    gpgcheck      => 1,
    repo_gpgcheck => 1,
  }

  ~> package { 'kubectl':
    ensure => present,
  }

  file_line { 'kubectl-alias':
    path => '/home/vagrant/.bashrc',
    line => 'alias k=kubectl',
  }

  file_line { 'k switch namespace':
    path => '/home/vagrant/.bashrc',
    line => 'ksn() { kubectl config set-context $(kubectl config current-context) --namespace=$1 ; }',
  }

  # install helm
  $helm_version = $versions[helm]
  $helm_file = "helm-v${helm_version}-linux-amd64.tar.gz"
  $helm_url = "https://storage.googleapis.com/kubernetes-helm/${helm_file}"
  $helm_dir = "/opt/helm-${helm_version}"
  $helm_exe = "${helm_dir}/helm"

  file { $helm_dir:
    ensure  => directory,
    mode    => '0711',
  } ~>

  archive { $helm_file:
    path            => "/tmp/${helm_file}",
    source          => $helm_url,
    extract         => true,
    extract_path    => $helm_dir,
    extract_command => 'tar xfz %s --strip-components=1',
    creates         => $helm_exe,
    cleanup         => true,
  } ~>

  file { '/usr/bin/helm':
    ensure => 'link',
    target => $helm_exe,
  }

  # install draft
  $draft_version = $versions[draft]
  $draft_file = "draft-v${draft_version}-linux-amd64.tar.gz"
  $draft_url = "https://azuredraft.blob.core.windows.net/draft/${draft_file}"
  $draft_dir = "/opt/draft-${draft_version}"
  $draft_exe = "${draft_dir}/draft"

  file { $draft_dir:
    ensure => directory,
    mode   => '0711',
  }

  ~> archive { $draft_file:
    path            => "/tmp/${draft_file}",
    source          => $draft_url,
    extract         => true,
    extract_path    => $draft_dir,
    extract_command => 'tar xfz %s --strip-components=1',
    creates         => $draft_exe,
    cleanup         => true,
  }

  ~> file { '/usr/bin/draft':
    ensure => 'link',
    target => $draft_exe,
  }

}
