# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

# read settings file
require "yaml"
cd = File.dirname(__FILE__)
settings = YAML.load_file("#{cd}/provision/settings.yaml")

# check for requried plugins
settings["plugins"].each do |plugin|
  unless Vagrant.has_plugin?(plugin)
    raise "Missing plugin! Run: vagrant plugin install #{plugin}"
  end
end unless settings["plugins"].nil? || settings["plugins"].empty?
# check for required env vars
['HTTP_PROXY', 'HTTPS_PROXY', 'NO_PROXY'].each do |var|
  unless ENV[var] || ENV[var] == ""
    raise %{
      Missing #{var} environment variable!
      Export it into your bashrc: echo export #{var}=... >> ~/.bashrc
      Then source the bashrc: source ~/.bashrc
    }
  end
end

Vagrant.configure("2") do |config|
  # virtual box settings
  config.vm.box = settings["box"]
  config.vm.hostname = settings["hostname"]
  config.vm.provider :virtualbox do |vb|
    vb.name = settings["name"]
    vb.memory = settings["memory"]
    vb.cpus = settings["cpus"]
  end
  
  # synchronise repo with /vagrant
  config.vm.synced_folder ".", "/vagrant", type: "virtualbox"

  # internet proxy settings
  config.proxy.http = ENV["HTTP_PROXY"]
  config.proxy.https = ENV["HTTPS_PROXY"]
  config.proxy.no_proxy = ENV["NO_PROXY"]

  # sync user files
  [".bashrc", ".gitconfig", ".ssh/id_rsa", ".ssh/id_rsa.pub", ".ssh/known_hosts"].each do |file|
    config.vm.provision :shell, inline: "rm -f /home/vagrant/#{file}"
    config.vm.provision :file, source: "~/#{file}", destination: "~/#{file}"
    config.vm.provision :shell, inline: "chmod 600 /home/vagrant/#{file}"
  end

  # forward ports
  settings["ports"].each do |port|
    split = port.split(':')
    config.vm.network :forwarded_port, host: split[0], guest: split[1]
  end unless settings["ports"].nil? || settings["ports"].empty?

  # install puppet
  config.vm.provision :shell, inline: <<-SHELL
    if ! type -p puppet >/dev/null 2>&1; then
      rpm -Uvh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
      yum install -y puppet-agent
    fi
  SHELL

  # install puppet modules
  settings["modules"].each do |mod|
    config.vm.provision :shell, inline: "puppet module install #{mod}"
  end unless settings["modules"].nil? || settings["modules"].empty?

  # apply default puppet manifest
  config.vm.provision :shell, inline: "puppet apply /vagrant/provision/default.pp"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
